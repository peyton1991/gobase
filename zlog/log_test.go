package zlog

import (
	"fmt"
	"regexp"
	"testing"
)

// 将字符串解析为浮点数，使用 IEEE754 规范进行舍入。
// bigSize 取值有 32 和 64 两种，表示转换结果的精度。
// 如果有语法错误，则 err.Error = ErrSyntax
// 如果结果超出范围，则返回 ±Inf，err.Error = ErrRange
func TestFatal(t *testing.T) {
	searchIn := "John: 2578.34 William: 4567.23 Steve: 5632.18"
	pat := "[0-9]+.[0-9]+" //正则
	f := func(s string) string {
		//v, _ := strconv.ParseFloat(s, 32)
		//return strconv.FormatFloat(v*2, 'f', 2, 32)
		return "*"
	}

	if ok, _ := regexp.Match(pat, []byte(searchIn)); ok {
		fmt.Println("match found")
	}

	re, _ := regexp.Compile(pat)
	//将匹配到的部分替换为"##.#"
	str := re.ReplaceAllString(searchIn, "##.#")
	fmt.Println(str)
	//参数为函数时候
	str2 := re.ReplaceAllStringFunc(searchIn, f)
	fmt.Println(str2)
}
