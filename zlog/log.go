package zlog

import (
	"go.uber.org/zap"
	"log"
)

type (
	Field  = zap.Field
	Logger struct {
		desugar *zap.Logger
		sugar   *zap.SugaredLogger
	}
)

var (
	LLogger *Logger
)

func GetLogger() *Logger {
	if LLogger == nil {
		log.Fatal("日志框架未进行初始化！")
	}
	return LLogger
}

func Info(msg string, fields ...Field) {
	GetLogger().desugar.Info(msg, fields...)
}

func Infow(msg string, keysAndValues ...interface{}) {
	GetLogger().sugar.Infow(msg, keysAndValues)
}

func Infof(template string, args ...interface{}) {
	GetLogger().sugar.Infof(template, args)
}

func Debug(msg string, fields ...Field) {
	GetLogger().desugar.Debug(msg, fields...)
}

func Debugw(msg string, keysAndValues ...interface{}) {
	GetLogger().sugar.Debugw(msg, keysAndValues)
}

func Debugf(template string, args ...interface{}) {
	GetLogger().sugar.Debugf(template, args)
}

func Warn(msg string, fields ...Field) {
	GetLogger().desugar.Warn(msg, fields...)
}

func Warnw(msg string, keysAndValues ...interface{}) {
	GetLogger().sugar.Warnw(msg, keysAndValues)
}

func Warnf(template string, args ...interface{}) {
	GetLogger().sugar.Warnf(template, args)
}

func Error(msg string, fields ...Field) {
	GetLogger().desugar.Error(msg, fields...)
}

func Errorw(msg string, keysAndValues ...interface{}) {
	GetLogger().sugar.Errorw(msg, keysAndValues)
}

func Errorf(template string, args ...interface{}) {
	GetLogger().sugar.Errorf(template, args)
}

func Panic(msg string, fields ...Field) {
	GetLogger().desugar.Panic(msg, fields...)
}

func Panicw(msg string, keysAndValues ...interface{}) {
	GetLogger().sugar.Panicw(msg, keysAndValues)
}

func Panicf(template string, args ...interface{}) {
	GetLogger().sugar.Panicf(template, args)
}

func Fatal(msg string, fields ...Field) {
	GetLogger().desugar.Fatal(msg, fields...)
}

func Fatalw(msg string, keysAndValues ...interface{}) {
	GetLogger().sugar.Fatalw(msg, keysAndValues)
}

func Fatalf(template string, args ...interface{}) {
	GetLogger().sugar.Fatalf(template, args)
}

func Flush() error {
	return GetLogger().desugar.Sync()
}
