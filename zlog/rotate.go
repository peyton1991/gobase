package zlog

import (
	"gopkg.in/natefinch/lumberjack.v2"
	"io"
)

// newRotate 日志切割功能
func newRotate(c *Config) io.Writer {
	return &lumberjack.Logger{
		Filename:   c.FileName,  // 日志文件路径
		MaxSize:    c.MaxSize,   // 每个日志文件保存的最大尺寸 单位：M
		MaxBackups: c.MaxBackup, // 日志文件最多保存多少个备份
		MaxAge:     c.MaxAge,    // 文件最多保存多少天
		LocalTime:  true,
		Compress:   c.Compress, // 是否压缩
	}
}
