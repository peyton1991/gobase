package zlog

import (
	"fmt"
	"go.uber.org/zap"
	"strings"
	"time"
)

func FieldErr(err error) Field {
	return zap.Error(err)
}

func FieldMethod(value string) Field {
	return zap.String("method", value)
}

func FieldCost(value time.Duration) Field {
	return zap.String("cost", fmt.Sprintf("%.3f", float64(value.Round(time.Microsecond))/float64(time.Millisecond)))
}

func FieldMod(value string) Field {
	value = strings.Replace(value, " ", ".", -1)
	return zap.String("mod", value)
}

func FieldAddr(value string) Field {
	return zap.String("addr", value)
}

func FieldAny(key string, object interface{}) Field {
	return zap.Any(key, object)
}

//func FieldContext(ctx context.Context) Field {
//	if tracing.SpanFromContext(ctx) == nil {
//		return zap.Any("traceId", "-")
//	}
//	traceId := tracing.SpanFromContext(ctx).Context().(jaeger.SpanContext).TraceID()
//	return zap.Any("traceId", traceId)
//}
