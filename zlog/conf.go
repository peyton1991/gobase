package zlog

import (
	"github.com/imdario/mergo"
	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
	"io"
	"os"
	"time"
)

// Config 日志配置
type Config struct {
	FileName      string `json:"fileName" yaml:"fileName"`           // 全路径文件名
	DisableCaller bool   `json:"disableCaller" yaml:"disableCaller"` // 是否添加调用者信息
	MaxSize       int    `json:"maxSize" yaml:"maxSize"`             // 日志输出文件最大长度，超过改值则截断
	MaxAge        int    `json:"maxAge" yaml:"maxAge"`               // 文件最多保存多少天
	MaxBackup     int    `json:"maxBackup" yaml:"maxBackup"`         // 日志文件最多保存多少个备份
	CallerSkip    int    `json:"callerSkip" yaml:"callerSkip"`       // CallerSkip
	Debug         bool   `json:"debug" yaml:"debug"`                 // 是否打印日志到控制台
	Compress      bool   `json:"compress"yaml:"compress"`            // 是否压缩
}

func DefaultConfig() Config {
	return Config{
		FileName:      "default.log",
		DisableCaller: false,
		MaxSize:       500,
		MaxAge:        1,
		MaxBackup:     1,
		CallerSkip:    1,
		Compress:      false,
	}
}

func init() {
	conf := DefaultConfig()
	conf.Debug = true
	newLogger(&conf)
}

// 初始化全局的日志框架
func Init(config *Config) {
	err := mergo.Merge(config, DefaultConfig())
	if err != nil {
		panic("get errors when merge log config")
	}

	newLogger(config)
}

func newLogger(config *Config) {
	zapOptions := make([]zap.Option, 0)
	zapOptions = append(zapOptions, zap.AddStacktrace(zap.DPanicLevel))
	if !config.DisableCaller {
		zapOptions = append(zapOptions, zap.AddCaller(), zap.AddCallerSkip(config.CallerSkip))
	}

	encoder := initEncoder()
	debugLevel := zap.LevelEnablerFunc(func(lvl zapcore.Level) bool {
		return lvl >= zapcore.DebugLevel
	})

	var hook io.Writer
	if config.Debug {
		hook = os.Stdout
	} else {
		hook = newRotate(config)
	}

	core := zapcore.NewTee(
		zapcore.NewCore(encoder, zapcore.AddSync(hook), debugLevel),
	)

	logger := zap.New(core, zapOptions...)
	LLogger = &Logger{
		desugar: logger,
		sugar:   logger.Sugar(),
	}
}

//初始化Encoder
func initEncoder() zapcore.Encoder {
	return zapcore.NewJSONEncoder(zapcore.EncoderConfig{
		MessageKey:    "msg",
		LevelKey:      "level",
		TimeKey:       "time",
		CallerKey:     "file",
		StacktraceKey: "stack",
		EncodeLevel:   zapcore.CapitalLevelEncoder, //基本zapcore.LowercaseLevelEncoder。将日志级别字符串转化为小写
		EncodeTime: func(t time.Time, enc zapcore.PrimitiveArrayEncoder) {
			enc.AppendString(t.Format("2006-01-02 15:04:05"))
		},
		EncodeCaller: zapcore.ShortCallerEncoder, //一般zapcore.ShortCallerEncoder，以包/文件:行号 格式化调用堆栈
		EncodeDuration: func(d time.Duration, enc zapcore.PrimitiveArrayEncoder) { //一般zapcore.SecondsDurationEncoder,执行消耗的时间转化成浮点型的秒
			enc.AppendInt64(int64(d) / 1000000)
		},
	})
}
