package znotify

import (
	"errors"
	"fmt"
	"github.com/imdario/mergo"
)

type Config struct {
	MailConfig
}

type MailConfig struct {
	MailUser            string `json:"mailUser"`
	MailPasswd          string `json:"mailPasswd"`
	MailHost            string `json:"mailHost"`
	MailPort            string `json:"mailPort"`
	MailUserAlias       string `json:"mailUserAlias"`
	MailSetCharset      string `json:"mailSetCharset"`
	MailBodyContentType string `json:"mailBodyContentType"`
}

func DefaultConfig() Config {
	return Config{MailConfig{
		MailUser:            "xx@xx",
		MailPasswd:          "passwd",
		MailHost:            "smtp.exmail.qq.com",
		MailPort:            "587",
		MailUserAlias:       "DevOps",
		MailSetCharset:      "utf-8",
		MailBodyContentType: "text/html",
	}}
}

func init() {
	Init(&Config{})
}

func Init(config *Config) (err error) {
	err = mergo.Merge(config, DefaultConfig())
	if err != nil {
		err = errors.New(fmt.Sprint("get error when merge gin config", err))
		return
	}
	return
}
