package znotify

import (
	"fmt"

	//  "github.com/opesun/goquery"
	log "gitee.com/peyton1991/gobase/zlog"
	"testing"
)

func TestSendMail(t *testing.T) {
	//定义收件人
	//var NotifyConfig=&Config{}
	//Init(NotifyConfig)
	mailTo := []string{
		"xxxx@xxxx",
	}
	//var url= "https://m.gmw.cn/baijia/2021-08/29/1302519813.html"
	//_,err:= goquery.ParseUrl(url)
	//if err!=nil{
	//	t.Log(err)
	//	t.Errorf("Exception accessing web page fail")
	//	return
	//}
	//邮件主题为"Hello"
	//subject := p.Find("title").Text()
	var conf = &Config{MailConfig{
		MailUser:            "xxx",
		MailPasswd:          "xxx",
		MailHost:            "xxx",
		MailPort:            "xxx",
		MailUserAlias:       "xxx",
		MailSetCharset:      "xxx",
		MailBodyContentType: "xxx",
	}}
	if err := Init(conf); err != nil {
		log.Panic(fmt.Sprint(err))
	}
	subject := "123"
	// 邮件正文
	body := "<h1>Hello From Go Mail hhhhh</h1>"
	//body := p.Html()
	t.Log(subject)
	//t.Log(body)
	err := conf.SendMail(mailTo, subject, body)
	if err != nil {
		t.Log(err)
		t.Errorf("send fail")
		return
	}

	t.Log("send successfully")
}
