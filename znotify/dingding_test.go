package znotify

import (
	"testing"
)

func TestSendDingDingText(t *testing.T) {
	//初始化钉钉客户端
	dingding := NewDingDing("https://oapi.dingtalk.com", "", "")

	body := "this is a test text"
	t.Log(body)
	//var userIdsAt = []string{"xxx"}
	//发送text类型钉钉
	var params = DingDingMessageText{
		DingDingMessageBase: DingDingMessageBase{
			At: DingDingAt{
				AtMobiles: []string{"xxx"},
				AtUserIds: nil,
				IsAtAll:   false,
			},
			Msgtype: "text",
		},
		Text: DingDingText{Content: "this is a test text"},
	}
	err := dingding.DingDingSendMessageText(params)
	if err != nil {
		t.Log(err)
		t.Errorf("send fail")
		return
	}

	t.Log("send successfully")
}

func TestSendDingDingLink(t *testing.T) {
	//初始化钉钉客户端
	dingding := NewDingDing("https://oapi.dingtalk.com", "cb2e4bd75a9d91d926e3402d8cae67b024ea649a0bc14d47dcb0def61c2b95f4", "SEC70759927e247df7a96e4c68372c44bfed00c272a6c35a2df6793bd24dd1ba378")

	body := "this is a test text"
	t.Log(body)
	//发送text类型钉钉
	var params = DingDingMessageLink{
		DingDingMessageBase: DingDingMessageBase{
			At: DingDingAt{
				AtMobiles: nil,
				AtUserIds: nil,
				IsAtAll:   false,
			},
			Msgtype: "link",
		},
		Link: DingDingLink{
			Text:       "服务名称：app-apk\n版本：v222.1\n下载密码：GT3nQ8o3xgxm5sE6\n下载有效期：2022-07-39",
			Title:      "apk信息",
			PicUrl:     "https://gitee.com/xlphp/php_poster_generation_class/raw/master/img/01.jpg",
			MessageUrl: "http://p.seafile.com/f/6d983fc0f5b04162bddb/?dl=1",
		},
	}

	err := dingding.DingDingSendMessageLink(params)
	if err != nil {
		t.Log(err)
		t.Errorf("send fail")
		return
	}

	t.Log("send successfully")
}

func TestSendDingDingMarkdown(t *testing.T) {
	//初始化钉钉客户端
	dingding := NewDingDing("https://oapi.dingtalk.com", "cb2e4bd75a9d91d926e3402d8cae67b024ea649a0bc14d47dcb0def61c2b95f4", "SEC70759927e247df7a96e4c68372c44bfed00c272a6c35a2df6793bd24dd1ba378")

	body := "this is a test text"
	t.Log(body)
	//发送text类型钉钉
	var params = DingDingMessageMarkdown{
		DingDingMessageBase: DingDingMessageBase{
			At: DingDingAt{
				AtMobiles: nil,
				AtUserIds: nil,
				IsAtAll:   false,
			},
			Msgtype: "markdown",
		},
		Markdown: DingDingMarkdown{
			Title: "app下载信息",
			Text:  "![](https://gitee.com/peyton1991/public/raw/master/images/app.png)\n# **apk下载信息**\n- 服务名称：app-apk \n- 版本：v222.1\n- 下载密码：GT3nQ8o3xgxm5sE6\n- 下载有效期：2022-07-39\n## [*点此下载*](http://p.seafile.com/f/6d983fc0f5b04162bddb/?dl=1)",
		},
	}

	err := dingding.DingDingSendMessageMarkdown(params)
	if err != nil {
		t.Log(err)
		t.Errorf("send fail")
		return
	}

	t.Log("send successfully")
}
