package znotify

import (
	"bytes"
	"crypto/hmac"
	"crypto/sha256"
	"encoding/base64"
	"encoding/json"
	"fmt"
	"gitee.com/peyton1991/gobase/zlog"
	"io"
	"net/http"
	"net/url"
	"time"
)

//钉钉机器人

type DingDingAt struct {
	AtMobiles []string `json:"atMobiles"`
	AtUserIds []string `json:"atUserIds"`
	IsAtAll   bool     `json:"isAtAll"`
}

type DingDingMessageBase struct {
	At      DingDingAt `json:"at"`
	Msgtype string     `json:"msgtype"`
}
type DingDingText struct {
	Content string `json:"content"`
}

type DingDingMessageText struct {
	DingDingMessageBase
	Text DingDingText `json:"text"`
}

type DingDingLink struct {
	Text       string `json:"text"`
	Title      string `json:"title"`
	PicUrl     string `json:"picUrl"`
	MessageUrl string `json:"messageUrl"`
}

type DingDingMessageLink struct {
	DingDingMessageBase
	Link DingDingLink `json:"link"`
}

type DingDingMarkdown struct {
	Title string `json:"title"`
	Text  string `json:"text"`
}

type DingDingMessageMarkdown struct {
	DingDingMessageBase
	Markdown DingDingMarkdown `json:"markdown"`
}

type dingDingAuth struct {
	DingDingSecret      string //钉钉加签
	DingDingAccessToken string //钉钉access token
	DingDingApiAddress  string //钉钉api地址
}

func NewDingDing(dingDingApiAddress, dingDingAccessToken, dingDingSecret string) *dingDingAuth {
	return &dingDingAuth{
		DingDingSecret:      dingDingSecret,
		DingDingAccessToken: dingDingAccessToken,
		DingDingApiAddress:  dingDingApiAddress,
	}
}

//api request
func (cli *dingDingAuth) dingDingRequest(method, uri string, header http.Header, body io.Reader) (*http.Response, error) {

	req, err := http.NewRequest(method, uri, body)

	if err != nil {
		return nil, fmt.Errorf("创建请求错误:%s", err)
	}
	fmt.Println("请求url:", uri)

	//如果外部传入Header则设置
	for k, v := range header {
		for _, vv := range v {
			req.Header.Add(k, vv)
		}
	}
	// fmt.Printf("访问url:%s\n", uri)
	return http.DefaultClient.Do(req)
}

//获取钉钉签名信息
func (cli *dingDingAuth) getDingDingSignTime() (int64, string, error) {
	timestamp := time.Now().UnixMilli()
	stringToSign := fmt.Sprintf("%d\n%s", timestamp, cli.DingDingSecret)
	hash := hmac.New(sha256.New, []byte(cli.DingDingSecret))
	hash.Write([]byte(stringToSign))
	signData := hash.Sum(nil)
	sign := url.QueryEscape(base64.StdEncoding.EncodeToString(signData))
	return timestamp, sign, nil
}

//发送消息
func (cli *dingDingAuth) dingDingSendMessage(query interface{}) (err error) {
	var params = url.Values{}
	var urlAddr string
	params.Set("access_token", cli.DingDingAccessToken)
	if cli.DingDingSecret != "" {
		timestamp, sign, _ := cli.getDingDingSignTime()
		urlAddr = fmt.Sprintf("%s/robot/send?%s&timestamp=%d&sign=%s", cli.DingDingApiAddress, params.Encode(), timestamp, sign)
	} else {
		urlAddr = fmt.Sprintf("%s/robot/send?%s", cli.DingDingApiAddress, params.Encode())
	}
	header := http.Header{"Content-Type": {"application/json"}}

	bodyByte, err := json.Marshal(query)
	if err != nil {
		zlog.Error(fmt.Sprint(err))
		return
	}
	body := bytes.NewBuffer(bodyByte)

	resp, err := cli.dingDingRequest("POST", urlAddr, header, body)
	if err != nil {
		zlog.Error(fmt.Sprintf("发送钉钉通知异常：%s", err))
		return
	}
	if resp.StatusCode != 200 {
		zlog.Error(fmt.Sprint("发送钉钉通知异常"))
	}
	zlog.Info("发送钉钉通知成功")
	return
}

//发送text类型
func (cli *dingDingAuth) DingDingSendMessageText(dingDingMessageText DingDingMessageText) (err error) {
	dingDingMessageText.Msgtype = "text"
	return cli.dingDingSendMessage(dingDingMessageText)
}

//发送link类型
func (cli *dingDingAuth) DingDingSendMessageLink(dingDingMessageLink DingDingMessageLink) (err error) {
	dingDingMessageLink.Msgtype = "link"
	return cli.dingDingSendMessage(dingDingMessageLink)
}

//发送markdown类型
func (cli *dingDingAuth) DingDingSendMessageMarkdown(dingDingMessageMarkdown DingDingMessageMarkdown) (err error) {
	dingDingMessageMarkdown.Msgtype = "markdown"
	return cli.dingDingSendMessage(dingDingMessageMarkdown)
}
