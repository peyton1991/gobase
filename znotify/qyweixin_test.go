package znotify

import "testing"

const qyweixinSecret = "xxxxx"
const qyweixinCorpId = "xxxxx"
const qyweixinApiAddress = "https://qyapi.weixin.qq.com"

func TestQyWeiXin_QyWeiXinAccessToken(t *testing.T) {
	qyWeiXin := NewQyWeiXin(qyweixinSecret, qyweixinCorpId, qyweixinApiAddress)
	accessToken, expiresIn, errs := qyWeiXin.QyWeiXinAccessToken()
	if errs != nil {
		t.Error(errs)
	}
	t.Log("expiresIn:", expiresIn, "accessToken:", accessToken)
	t.Log("TestQyWeiXin_QyWeiXinAccessToken success")
}

func TestQyWeiXin_QyWeiXinSendMessage(t *testing.T) {
	qyWeiXin := NewQyWeiXin(qyweixinSecret, qyweixinCorpId, qyweixinApiAddress)
	var accessToken = "6HBKPgYiJ-MwNKbABp-LxzD7CIGQQE0EReVFrvdW-VxlDUhha-LW0HPQdYusCxh7mB8Vj0w1Obi6bDdIas9aMaOJG3hgi-iSv2t38J0VAx23jqSR2obwgdeWu6af1eEc8ePsk8Ht__1OqTj-B9QJtRRQjkDtK0KUH1qCr0-Hm8zh8ANxPVEiV7AonqsFY1SLy7E2ct24I5q1_QVQ"
	content := `
告警类型: Prd-PodCPUUsage
=====================
===告警恢复===
告警详情: namespace: prd-data-platform下的pod prd-driller-deploy-6d6f669695-w7f8s: CPU使用率超过85%，podCPU使用率为：86.52257931926357%
告警开始时间: 2021-09-01 13:37:18
恢复时间: 2021-09-01 13:39:18
===参考信息===
故障pod名称: prd-driller-deploy-6d6f669695-w7f8s;
=====================
`
	qyWeiXinSendMessageResponse, errs := qyWeiXin.QyWeiXinSendMessageText(1000008, accessToken, content, "UserID1|UserID2")
	if errs != nil {
		t.Log(errs)
	}
	//判断是否超时，若超时刷新 accessToken
	var err error
	if qyWeiXinSendMessageResponse != nil && qyWeiXinSendMessageResponse.ErrorCode == 42001 {
		accessToken, _, err = qyWeiXin.QyWeiXinAccessToken()
		if err != nil {
			t.Error(err)
			return
		}
		t.Log("repeat second")
		qyWeiXinSendMessageResponse, err = qyWeiXin.QyWeiXinSendMessageText(1000008, accessToken, content, "@all")
		if errs != nil {
			t.Error(err)
			return
		}
	}
	t.Log("qyWeiXinSendMessageResponse:", qyWeiXinSendMessageResponse)
	t.Log("TestQyWeiXin_QyWeiXinSendMessage success")
}

func TestQyWeiXin_QyWeiXinGetDepartmentMembers(t *testing.T) {
	qyWeiXin := NewQyWeiXin(qyweixinSecret, qyweixinCorpId, qyweixinApiAddress)
	var accessToken = "YVxX2l4pVdcw3o1_3v-tjEkz7vYnLgHqVqEM_zU6DNhL5T35gGGe9eKyqc1VLnFU0AnQx1b8lS43sUCw8Wb-9agtIOtnZ3rJApfB4N6iZG4Mtyb5eFaVR29pK0NIURS14QAUZzMIwkp3UhADrz0QIFTYIlle67J6feXYQl4lCTnwGV4hX09GTCxe3VqSjJuL2KVFEUy44OzXem8o9oBoXQ"
	qyWeiXinGetDepartmentMembersResponse, err := qyWeiXin.QyWeiXinGetDepartmentMembers(accessToken, 2)
	if err != nil {
		t.Error(err)
	}
	t.Log(qyWeiXinGetDepartmentMembersResponse)
}
