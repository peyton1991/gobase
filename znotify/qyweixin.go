package znotify

import (
	"encoding/json"
	"errors"
	"fmt"
	"gitee.com/peyton1991/gobase/zlog"
	//"github.com/parnurzeal/gorequest"
	"github.com/valyala/fasthttp"
)

type qyWeiXin struct {
	QyweixinSecret     string
	QyweixinCorpId     string
	QyweixinApiAddress string
}

func NewQyWeiXin(qyweixinSecret, qyweixinCorpId, qyweixinApiAddress string) *qyWeiXin {
	return &qyWeiXin{
		QyweixinSecret:     qyweixinSecret,
		QyweixinCorpId:     qyweixinCorpId,
		QyweixinApiAddress: qyweixinApiAddress,
	}
}

type QyWeiXinAccessTokenStruct struct {
	ErrorCode   int64  `json:"errcode"`
	ErrMsg      string `json:"errmsg"`
	AccessToken string `json:"access_token"`
	ExpiresIn   int64  `json:"expires_in"`
}

type QyWeiXinSendMessageResponse struct {
	ErrorCode    int64  `json:"errcode"`
	ErrMsg       string `json:"errmsg"`
	InvalidUser  string `json:"invalid_user"`
	InvalidParty string `json:"invalid_party"`
	MsgId        string `json:"msg_id"`
	ResponseCode string `json:"response_code"`
}

type QyWeiXinDepartmentMemberList struct {
	UserId     string `json:"userid"`
	Name       string `json:"name"`
	Department []int  `json:"department"`
	OpenUserid string `json:"open_userid"`
}
type QyWeiXinGetDepartmentMembersResponse struct {
	ErrorCode int64                          `json:"errcode"`
	ErrMsg    string                         `json:"errmsg"`
	UserList  []QyWeiXinDepartmentMemberList `json:"userlist"`
}

type QyWeiXinSendMessageStruct struct {
	ToUser                 string `json:"touser"`
	ToParty                string `json:"toparty"`
	ToTag                  string `json:"totag"`
	MsgType                string `json:"msgtype"`
	AgentId                int    `json:"agentid"`
	Safe                   int    `json:"safe"`
	EnableIdTrans          int    `json:"enable_id_trans"`
	EnableDuplicateCheck   int    `json:"enable_duplicate_check"`
	DuplicateCheckInterval int    `json:"duplicate_check_interval"`
}

type QyWeiXinSendMessageContent struct {
	Content string `json:"content"`
}
type QyWeiXinSendMessageTextStruct struct {
	QyWeiXinSendMessageStruct
	Text QyWeiXinSendMessageContent `json:"text"`
}
type QyWeiXinSendMessageMarkdownStruct struct {
	QyWeiXinSendMessageStruct
	Markdown QyWeiXinSendMessageContent `json:"markdown"`
}

func (q *qyWeiXin) QyWeiXinAccessToken() (accessToken string, expiresIn int64, err error) {
	accessTokenUrl := fmt.Sprintf("%s/cgi-bin/gettoken?corpid=%s&corpsecret=%s", q.QyweixinApiAddress, q.QyweixinCorpId, q.QyweixinSecret)
	//request := gorequest.New()
	//_, body, errs := request.Get(accessTokenUrl).End()
	//if errs != nil {
	//	zlog.Error(fmt.Sprint(errs))
	//	return
	//}
	req := fasthttp.AcquireRequest()
	defer fasthttp.ReleaseRequest(req)
	req.Header.SetMethod("GET")
	req.SetRequestURI(accessTokenUrl)
	resp := fasthttp.AcquireResponse()
	defer fasthttp.ReleaseResponse(resp)

	if err = fasthttp.Do(req, resp); err != nil {
		zlog.Error(fmt.Sprint(err))
		return
	}
	body := resp.Body()
	var qyWeiXinAccessTokenStruct QyWeiXinAccessTokenStruct
	if err = json.Unmarshal(body, &qyWeiXinAccessTokenStruct); err != nil {
		zlog.Error(fmt.Sprint(err))
		return
	}
	accessToken = qyWeiXinAccessTokenStruct.AccessToken
	expiresIn = qyWeiXinAccessTokenStruct.ExpiresIn
	return
}

func (q *qyWeiXin) QyWeiXinSendMessageText(agentId int, accessToken, content string, toUser string) (qyWeiXinSendMessageResponse *QyWeiXinSendMessageResponse, err error) {
	sendMessageUrl := fmt.Sprintf("%s/cgi-bin/message/send?access_token=%s", q.QyweixinApiAddress, accessToken)
	//request := gorequest.New()
	var qyWeiXinSendMessageTextStruct = QyWeiXinSendMessageTextStruct{
		QyWeiXinSendMessageStruct: QyWeiXinSendMessageStruct{
			ToUser:                 toUser,
			ToParty:                "",
			ToTag:                  "",
			MsgType:                "text",
			AgentId:                agentId,
			Safe:                   0,
			EnableIdTrans:          0,
			EnableDuplicateCheck:   0,
			DuplicateCheckInterval: 0,
		},
		Text: QyWeiXinSendMessageContent{Content: content},
	}
	qyWeiXinSendMessageTextStructString, err := json.Marshal(qyWeiXinSendMessageTextStruct)
	if err != nil {
		zlog.Error(fmt.Sprint(err))
		return
	}

	//_, body, errs := request.Post(sendMessageUrl).Set("Content-Type", "application/json").Send(string(qyWeiXinSendMessageTextStructString)).End()
	//if errs != nil {
	//	zlog.Error(fmt.Sprint(errs))
	//	return
	//}
	req := fasthttp.AcquireRequest()
	defer fasthttp.ReleaseRequest(req)
	req.Header.SetMethod("POST")
	req.SetRequestURI(sendMessageUrl)
	req.Header.SetContentType("application/json")
	req.SetBody(qyWeiXinSendMessageTextStructString)
	resp := fasthttp.AcquireResponse()
	defer fasthttp.ReleaseResponse(resp)

	if err = fasthttp.Do(req, resp); err != nil {
		zlog.Error(fmt.Sprint(err))
		return
	}
	body := resp.Body()
	err = json.Unmarshal(body, &qyWeiXinSendMessageResponse)
	if err != nil {
		zlog.Error(fmt.Sprint(err))
		return
	}
	if qyWeiXinSendMessageResponse.ErrorCode != 0 {
		err = errors.New(qyWeiXinSendMessageResponse.ErrMsg)
		zlog.Error(fmt.Sprint(qyWeiXinSendMessageResponse.ErrMsg))
		return
	}
	return
}

func (q *qyWeiXin) QyWeiXinGetDepartmentMembers(accessToken string, departmentId int) (qyWeiXinGetDepartmentMembersResponse *QyWeiXinGetDepartmentMembersResponse, err error) {
	getDepartmentMembersMessageUrl := fmt.Sprintf("%s/cgi-bin/user/simplelist?access_token=%s&department_id=%d&fetch_child=0", q.QyweixinApiAddress, accessToken, departmentId)
	//request := gorequest.New()
	//_, body, errs := request.Get(getDepartmentMembersMessageUrl).End()
	//zlog.Infof("body:", body)
	//err := json.Unmarshal([]byte(body), &qyWeiXinGetDepartmentMembersResponse)
	//if err != nil {
	//	errs = append(errs, err)
	//	zlog.Error(fmt.Sprint(errs))
	//	return
	//}
	req := fasthttp.AcquireRequest()
	defer fasthttp.ReleaseRequest(req)
	req.Header.SetMethod("GET")
	req.SetRequestURI(getDepartmentMembersMessageUrl)
	resp := fasthttp.AcquireResponse()
	defer fasthttp.ReleaseResponse(resp)
	if err = fasthttp.Do(req, resp); err != nil {
		zlog.Error(fmt.Sprint(err))
		return
	}
	body := resp.Body()
	err = json.Unmarshal(body, &qyWeiXinGetDepartmentMembersResponse)
	if err != nil {
		zlog.Error(fmt.Sprint(err))
		return
	}
	return
}
