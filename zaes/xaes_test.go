package zaes

import (
	"fmt"
	"testing"

	"github.com/stretchr/testify/assert"
)

var (
	secretKey = "/Oh0jFlOhZ0dDNa4t/Y6EmW0EHIKKavj"
)

func TestEncrypt(t *testing.T) {
	assert := assert.New(t)
	password := "123456"
	SetAESSecret(secretKey)
	secret, err := Encrypt(password)
	t.Log(secret)
	fmt.Println(secret, "-----")
	assert.Equal(nil, err, "they should be equal")
	msg, err := Decrypt("l/eqyxezD6ERUbuSs7c6hjd1+PY1ZESRKA3UPb5ECQ0=")
	fmt.Println(msg)
	assert.Equal(nil, err, "they should be equal")

}
