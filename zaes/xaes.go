package zaes

import (
	"bytes"
	"encoding/base64"
)

var (
	secret []byte
)

// SetAESSecret 设置全局的对称秘钥，强度要求256位
func SetAESSecret(key string) {

	if len(key) != 32 {
		panic("秘钥长度需要256位！")
	}
	secret = []byte(key)
}

// PKCS7Padding 使用PKCS7进行填充，IOS也是7
func PKCS7Padding(ciphertext []byte, blockSize int) []byte {
	padding := blockSize - len(ciphertext)%blockSize
	padtext := bytes.Repeat([]byte{byte(padding)}, padding)
	return append(ciphertext, padtext...)
}

func PKCS7UnPadding(origData []byte) []byte {
	length := len(origData)
	unpadding := int(origData[length-1])
	return origData[:(length - unpadding)]
}

func Encrypt(plaintext string) (string, error) {
	if secret == nil {
		panic("未初始化秘钥，使用xeas.SetAESSecret(key string)初始化秘钥")
	}
	data, err := AesCBCEncrypt([]byte(plaintext), secret)
	if err != nil {
		return "", err
	}
	return base64.StdEncoding.EncodeToString(data), nil
}

func Decrypt(ciphertext string) (string, error) {
	if ciphertext == "" {
		return "", nil
	}
	if secret == nil {
		panic("未初始化秘钥，使用xeas.SetAESSecret(key string)初始化秘钥")
	}
	data, err := base64.StdEncoding.DecodeString(ciphertext)
	if err != nil {
		return "", err
	}
	dnData, err := AesCBCDecrypt(data, secret)
	if err != nil {
		return "", err
	}
	return string(dnData), nil
}

func init() {
	SetAESSecret("/Oh0jFlOhZ0dDNa4t/Y6EmW0EHIKKavj")
}
