/*
	本包主要用于数据库、消息队列和缓存(如：redis)的秘钥加密。默认使用的是对称加密AES的CBC模式
	秘钥强度要求为256位，否则直接抛出异常。
	使用方式如下：
	设置秘钥：
	xaes.SetAESSecret(key)
	加密: ciphertext,err:=xaes.Encrypt(plaintext)
	解密: plaintext,err:=xaes.Decrypt(ciphertext)

*/

package zaes
