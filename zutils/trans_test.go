package zutils

import ("testing")

func Test_Trans(t *testing.T) {
	str := String("tea")
	strVal := StringValue(str)
	AssertEqual(t, "tea", strVal)
	AssertEqual(t, "", StringValue(nil))

	strSlice := StringSlice([]string{"tea"})
	strSliceVal := StringSliceValue(strSlice)
	AssertEqual(t, []string{"tea"}, strSliceVal)
	AssertNil(t, StringSlice(nil))
	AssertNil(t, StringSliceValue(nil))

	b := Bool(true)
	bVal := BoolValue(b)
	AssertEqual(t, true, bVal)
	AssertEqual(t, false, BoolValue(nil))

	bSlice := BoolSlice([]bool{false})
	bSliceVal := BoolSliceValue(bSlice)
	AssertEqual(t, []bool{false}, bSliceVal)
	AssertNil(t, BoolSlice(nil))
	AssertNil(t, BoolSliceValue(nil))

	f64 := Float64(2.00)
	f64Val := Float64Value(f64)
	AssertEqual(t, float64(2.00), f64Val)
	AssertEqual(t, float64(0), Float64Value(nil))

	f32 := Float32(2.00)
	f32Val := Float32Value(f32)
	AssertEqual(t, float32(2.00), f32Val)
	AssertEqual(t, float32(0), Float32Value(nil))

	f64Slice := Float64Slice([]float64{2.00})
	f64SliceVal := Float64ValueSlice(f64Slice)
	AssertEqual(t, []float64{2.00}, f64SliceVal)
	AssertNil(t, Float64Slice(nil))
	AssertNil(t, Float64ValueSlice(nil))

	f32Slice := Float32Slice([]float32{2.00})
	f32SliceVal := Float32ValueSlice(f32Slice)
	AssertEqual(t, []float32{2.00}, f32SliceVal)
	AssertNil(t, Float32Slice(nil))
	AssertNil(t, Float32ValueSlice(nil))

	i := Int(1)
	iVal := IntValue(i)
	AssertEqual(t, 1, iVal)
	AssertEqual(t, 0, IntValue(nil))

	i8 := Int8(int8(1))
	i8Val := Int8Value(i8)
	AssertEqual(t, int8(1), i8Val)
	AssertEqual(t, int8(0), Int8Value(nil))

	i16 := Int16(int16(1))
	i16Val := Int16Value(i16)
	AssertEqual(t, int16(1), i16Val)
	AssertEqual(t, int16(0), Int16Value(nil))

	i32 := Int32(int32(1))
	i32Val := Int32Value(i32)
	AssertEqual(t, int32(1), i32Val)
	AssertEqual(t, int32(0), Int32Value(nil))

	i64 := Int64(int64(1))
	i64Val := Int64Value(i64)
	AssertEqual(t, int64(1), i64Val)
	AssertEqual(t, int64(0), Int64Value(nil))

	iSlice := IntSlice([]int{1})
	iSliceVal := IntValueSlice(iSlice)
	AssertEqual(t, []int{1}, iSliceVal)
	AssertNil(t, IntSlice(nil))
	AssertNil(t, IntValueSlice(nil))

	i8Slice := Int8Slice([]int8{1})
	i8ValSlice := Int8ValueSlice(i8Slice)
	AssertEqual(t, []int8{1}, i8ValSlice)
	AssertNil(t, Int8Slice(nil))
	AssertNil(t, Int8ValueSlice(nil))

	i16Slice := Int16Slice([]int16{1})
	i16ValSlice := Int16ValueSlice(i16Slice)
	AssertEqual(t, []int16{1}, i16ValSlice)
	AssertNil(t, Int16Slice(nil))
	AssertNil(t, Int16ValueSlice(nil))

	i32Slice := Int32Slice([]int32{1})
	i32ValSlice := Int32ValueSlice(i32Slice)
	AssertEqual(t, []int32{1}, i32ValSlice)
	AssertNil(t, Int32Slice(nil))
	AssertNil(t, Int32ValueSlice(nil))

	i64Slice := Int64Slice([]int64{1})
	i64ValSlice := Int64ValueSlice(i64Slice)
	AssertEqual(t, []int64{1}, i64ValSlice)
	AssertNil(t, Int64Slice(nil))
	AssertNil(t, Int64ValueSlice(nil))

	ui := Uint(1)
	uiVal := UintValue(ui)
	AssertEqual(t, uint(1), uiVal)
	AssertEqual(t, uint(0), UintValue(nil))

	ui8 := Uint8(uint8(1))
	ui8Val := Uint8Value(ui8)
	AssertEqual(t, uint8(1), ui8Val)
	AssertEqual(t, uint8(0), Uint8Value(nil))

	ui16 := Uint16(uint16(1))
	ui16Val := Uint16Value(ui16)
	AssertEqual(t, uint16(1), ui16Val)
	AssertEqual(t, uint16(0), Uint16Value(nil))

	ui32 := Uint32(uint32(1))
	ui32Val := Uint32Value(ui32)
	AssertEqual(t, uint32(1), ui32Val)
	AssertEqual(t, uint32(0), Uint32Value(nil))

	ui64 := Uint64(uint64(1))
	ui64Val := Uint64Value(ui64)
	AssertEqual(t, uint64(1), ui64Val)
	AssertEqual(t, uint64(0), Uint64Value(nil))

	uiSlice := UintSlice([]uint{1})
	uiValSlice := UintValueSlice(uiSlice)
	AssertEqual(t, []uint{1}, uiValSlice)
	AssertNil(t, UintSlice(nil))
	AssertNil(t, UintValueSlice(nil))

	ui8Slice := Uint8Slice([]uint8{1})
	ui8ValSlice := Uint8ValueSlice(ui8Slice)
	AssertEqual(t, []uint8{1}, ui8ValSlice)
	AssertNil(t, Uint8Slice(nil))
	AssertNil(t, Uint8ValueSlice(nil))

	ui16Slice := Uint16Slice([]uint16{1})
	ui16ValSlice := Uint16ValueSlice(ui16Slice)
	AssertEqual(t, []uint16{1}, ui16ValSlice)
	AssertNil(t, Uint16Slice(nil))
	AssertNil(t, Uint16ValueSlice(nil))

	ui32Slice := Uint32Slice([]uint32{1})
	ui32ValSlice := Uint32ValueSlice(ui32Slice)
	AssertEqual(t, []uint32{1}, ui32ValSlice)
	AssertNil(t, Uint32Slice(nil))
	AssertNil(t, Uint32ValueSlice(nil))

	ui64Slice := Uint64Slice([]uint64{1})
	ui64ValSlice := Uint64ValueSlice(ui64Slice)
	AssertEqual(t, []uint64{1}, ui64ValSlice)
	AssertNil(t, Uint64Slice(nil))
	AssertNil(t, Uint64ValueSlice(nil))
}
