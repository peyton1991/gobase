package zutils
import (
	"testing"

)

func Test_GetUUID(t *testing.T) {
	uuid := getUUID()
	AssertEqual(t, len(uuid), 32)
}
