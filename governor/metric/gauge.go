package metric

import "github.com/prometheus/client_golang/prometheus"

type GaugeVecOpts struct {
	Namespace string
	Subsystem string
	Name      string
	Help      string
	Labels    []string
}

type gaugeVec struct {
	gauge	*prometheus.GaugeVec
}

func NewGaugeVec(cfg *GaugeVecOpts) *gaugeVec {
	if cfg == nil {
		return nil
	}
	vec := prometheus.NewGaugeVec(
		prometheus.GaugeOpts{
			Namespace: cfg.Namespace,
			Subsystem: cfg.Subsystem,
			Name:      cfg.Name,
			Help:      cfg.Help,
		}, cfg.Labels)
	prometheus.MustRegister(vec)
	return &gaugeVec{
		gauge: vec,
	}
}

func (gauge *gaugeVec) Inc(labels ...string) {
	gauge.gauge.WithLabelValues(labels...).Inc()
}

func (gauge *gaugeVec) Dec( labels ...string) {
	gauge.gauge.WithLabelValues(labels...).Dec()
}

func (gauge *gaugeVec) Add(v float64, labels ...string) {
	gauge.gauge.WithLabelValues(labels...).Add(v)
}

func (gauge *gaugeVec) Set(v float64, labels ...string) {
	gauge.gauge.WithLabelValues(labels...).Set(v)
}
