package metric

import (
	"github.com/prometheus/client_golang/prometheus/promhttp"
	"net/http"
)

func Init(mux *http.ServeMux) {
	mux.Handle("/metrics", promhttp.Handler())
}
