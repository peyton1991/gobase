package metric

import "github.com/prometheus/client_golang/prometheus"

// HistogramVecOpts ...
type HistogramVecOpts struct {
	Namespace string
	Subsystem string
	Name      string
	Help      string
	Labels    []string
	Buckets   []float64
}

type histogramVec struct {
	histogram  *prometheus.HistogramVec
}

func NewHistogramVec(cfg *HistogramVecOpts) *histogramVec {
	if cfg == nil {
		return nil
	}
	vec := prometheus.NewHistogramVec(
		prometheus.HistogramOpts{
			Namespace: cfg.Namespace,
			Subsystem: cfg.Subsystem,
			Name:      cfg.Name,
			Help:      cfg.Help,
		}, cfg.Labels)
	prometheus.MustRegister(vec)
	return &histogramVec{
		histogram: vec,
	}
}
// Observe ...
func (histogram *histogramVec) Observe(v float64, labels ...string) {
	histogram.histogram.WithLabelValues(labels...).Observe(v)
}