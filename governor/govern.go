package governor

import (
	"github.com/pkg/errors"
	"gitee.com/peyton1991/gobase/governor/metric"
	"gitee.com/peyton1991/gobase/governor/pprof"
	"net/http"
	"sync"
)

var (
	_once sync.Once
)

type Config struct {
	Address string `json:"address" yaml:"address"` //服务治理地址
}

func Init(config *Config) {
	_once.Do(func() {
		mux := http.NewServeMux()

		pprof.Init(mux)

		metric.Init(mux)

		go func() {
			if err := http.ListenAndServe(config.Address, mux); err != nil {
				panic(errors.Errorf("govern: listen %s: error(%v)", config.Address, err))
			}
		}()
	})

}
