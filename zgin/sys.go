package zgin

import (
	"gitee.com/peyton1991/gobase/zaes"
	"github.com/gin-gonic/gin"
	log "gitee.com/peyton1991/gobase/zlog"
	"net/http"
)

func SysAuth(config *Config) gin.HandlerFunc {
	return func(c *gin.Context) {
		// 1.判断用户是否携带认证信息-jwtToken
		token := c.GetHeader("AuthorizationToken")
		if token == "" {
			c.JSON(http.StatusOK, response{Msg: "本次访问请求未携带身份认证信息", Code: http.StatusUnauthorized})
			c.Abort()
			return
		}
		info, err := zaes.Decrypt(token)
		if err != nil {
			c.JSON(http.StatusOK, response{Msg: "token解析异常", Code: http.StatusUnauthorized})
			c.Abort()
			return
		}
		log.Info(info)
		c.Next()
	}
}
