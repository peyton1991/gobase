package zgin

import (
	"context"
	"fmt"
	"gitee.com/peyton1991/gobase/zlog"
	"github.com/gin-gonic/gin"
	"github.com/imdario/mergo"
	"net/http"
	"os"
	"os/signal"
	"syscall"
)
type(
BaseHandle struct{}
Context     = gin.Context
Engine      = gin.Engine
HandlerFunc = gin.HandlerFunc
response struct{
	Code int  `json:"code"`
	Msg  string `json:"msg"`
	Data interface{} `json:"data,omitempty"`  //若为空，则不展示
}
)

type Config struct{
	Address       string `json:"address" yaml:"address"`             //gin服务的启动地址
	Mode          string `json:"mode" yaml:"mode"`                   //gin的启动模式
	Debug         bool   `json:"debug" yaml:"debug"`                 //是否打印调试日志
	SecretKey     string `json:"secretKey" yaml:"secretKey"`         //内部系统调用的对称秘钥
	DisableMetric bool   `json:"disableMetric" yaml:"disableMetric"` //是否进行监控
	DisableTrace  bool   `json:"disableTrace" yaml:"disableTrace"`   //是否进行链路追踪
	RateLimit     int    `json:"rateLimit" yaml:"rateLimit"`         //限速
}

type Server struct {
	*gin.Engine
	config *Config
	Server *http.Server
}
func DefaultConfig() Config {
	return Config{
		Address:       "127.0.0.1:8080",
		Mode:          gin.ReleaseMode,
		Debug:         false,
		DisableMetric: false,
		DisableTrace:  false,
		RateLimit:     0,
	}
}


func NEW(config *Config) *Server{
	err := mergo.Merge(config, DefaultConfig())
	if err != nil {
		zlog.Panic("get error when merge gin config", zlog.FieldMod("gin"), zlog.FieldErr(err))
	}
	g := gin.New()
	gin.SetMode(config.Mode)
	g.Use(gin.Recovery())
	//if !config.DisableTrace {
	//	g.Use(Trace(config))
	//}
	if config.Debug {
		g.Use(Debug(config))
	}
	if !config.DisableMetric {
		g.Use(Metric(config))
	}
	if config.RateLimit != 0 {
		g.Use(RateLimit(config))
	}
	server := &http.Server{
		Addr:    config.Address,
		Handler: g,
	}
	return &Server{
		Engine: g,
		config: config,
		Server: server,
	}
}
func SignalNotify(srv *Server) {
	ch := make(chan os.Signal)
	signal.Notify(
		ch,
		syscall.SIGHUP,
		syscall.SIGINT,
		syscall.SIGTERM,
		syscall.SIGQUIT,
		syscall.SIGKILL,
	)

	for {
		sig := <-ch
		switch sig {
		case syscall.SIGQUIT:
			srv.Server.Shutdown(context.TODO())
			return
		case syscall.SIGHUP, syscall.SIGINT, syscall.SIGTERM, syscall.SIGKILL:
			srv.Server.Close()
			return
		default:
			return
		}
	}
}





func result(c *gin.Context,code int,message string,data interface{}){
	c.JSON(http.StatusOK,response{
		Code: code,
		Msg:  message,
		Data: data,
	})
}

func (b *BaseHandle)OKWithMessage(c *gin.Context,message string){
	result(c,http.StatusOK,message,nil)
	zlog.Info(fmt.Sprintf("请求URL:%s 返回code:%d message:%s",c.Request.URL.Path,http.StatusOK,message))
	return
}
func (b *BaseHandle)FailWithMessage(c *gin.Context,message string){
	result(c,400,message,nil)
	zlog.Error(fmt.Sprintf("请求URL:%s 返回code:%d message:%s",c.Request.URL.Path,400,message))
	return
}
func (b *BaseHandle)OKWithData(c *gin.Context,message string,data interface{}){
	result(c,http.StatusOK,message,data)
	zlog.Info(fmt.Sprintf("请求URL:%s 返回code:%d message:%s",c.Request.URL.Path,http.StatusOK,message))
	return
}

