package zgin

import (
	"github.com/gin-gonic/gin"
"go.uber.org/ratelimit"
)

func RateLimit(config *Config) gin.HandlerFunc {
	return func(c *gin.Context) {
		rl := ratelimit.New(config.RateLimit)
		rl.Take()
		c.Next()
	}
}
