package zgin

import (
	"bytes"
	"fmt"
	log "gitee.com/peyton1991/gobase/zlog"
	"github.com/gin-gonic/gin"

	"io/ioutil"
	"time"
)

func Debug(config *Config) gin.HandlerFunc {
	return func(c *gin.Context) {
		var param gin.LogFormatterParams
		var data string
		start := time.Now()
		path := c.Request.URL.Path
		raw := c.Request.URL.RawQuery
		if c.GetHeader("Content-Type") == "application/json" {
			bodyBytes, _ := ioutil.ReadAll(c.Request.Body)
			data = string(bodyBytes)
			c.Request.Body.Close() //  must close
			c.Request.Body = ioutil.NopCloser(bytes.NewBuffer(bodyBytes))
		}
		c.Next()
		param.TimeStamp = time.Now()
		param.Latency = time.Now().Sub(start)
		param.ClientIP = c.ClientIP()
		param.Method = c.Request.Method
		param.StatusCode = c.Writer.Status()
		param.BodySize = c.Writer.Size()
		if raw != "" {
			path = path + "?" + raw
		}
		param.Path = path
		log.Info(fmt.Sprintf("[GIN] %v | %3d |%13v | %12s | %-7s %s | 请求参数为:%s",
			param.TimeStamp.Format("2006/01/02 - 15:04:05"), param.StatusCode,
			param.Latency,
			param.ClientIP,
			param.Method,
			param.Path,
			data,
		))
	}
}
