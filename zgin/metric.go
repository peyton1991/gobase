package zgin

import (
	"gitee.com/peyton1991/gobase/governor/metric"
	"github.com/gin-gonic/gin"
	"net/http"
	"time"
)

var (
	// api访问总数记录
	_webRequestTotal = metric.NewCounterVec(&metric.CounterVecOpts{
		Name:   "web_request_total",
		Help:   "Number of requests in total",
		Labels: []string{"method", "endpoint"},
	})

	// Histogram类型指标，bucket代表duration的分布区间
	_webRequestDuration = metric.NewHistogramVec(&metric.HistogramVecOpts{
		Name:    "web_request_duration_seconds",
		Help:    "web request duration distribution",
		Buckets: []float64{0.1, 0.3, 0.5, 0.7, 0.9, 1},
		Labels:  []string{"method", "endpoint"},
	})

	// 系统api的并发连接数
	_webRequestConnect = metric.NewGaugeVec(&metric.GaugeVecOpts{
		Name:   "web_request_connection",
		Help:   "Number of  current_connect in total",
		Labels: []string{"method", "endpoint"},
	})

	// 错误请求
	_webRequestErr = metric.NewCounterVec(&metric.CounterVecOpts{
		Name:   "web_request_total111",
		Help:   "Number of requests in total",
		Labels: []string{"method", "endpoint"},
	})
)

func Metric(config *Config) gin.HandlerFunc {
	return func(c *gin.Context) {
		start := time.Now()
		_webRequestConnect.Inc(c.Request.Method, c.Request.URL.Path)
		c.Next()
		cost := time.Since(start)
		_webRequestTotal.Inc(c.Request.Method, c.Request.URL.Path)
		_webRequestDuration.Observe(cost.Seconds(), c.Request.Method, c.Request.URL.Path)
		_webRequestConnect.Dec(c.Request.Method, c.Request.URL.Path)
		if c.Writer.Status() != http.StatusOK {
			_webRequestErr.Inc(c.Request.Method, c.Request.URL.Path)
		}
	}
}
